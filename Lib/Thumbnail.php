<?php

class Thumbnail {

/**
 * 
 *
 * @var PhpThumbFactory
 */
	protected $Factory;

/**
 *
 *
 * @var File
 */
	public $file;

/**
 * 
 *
 * @var array
 */
	public $settings = array();

/**
 * Constructor
 *
 * @param string
 */
	public function __construct($path) {
		$this->file = new File($path);
		//$this->file->copy(TMP . 'cache' . DS . 'images');
		$this->Factory = PhpThumbFactory::create($this->file->pwd());
	}

/**
 * 
 *
 * @param string 
 * @return string
 */
	public function getThumbnailPath() {
		return IMAGES . 'thumbnails' . DS . $this->file->name;
	}

/**
 * 
 *
 * @param string
 * @return string
 */
	public function getThumbnailUrl() {
		return 'thumbnails' . '/' . $this->getFilename();
	}

/**
 *
 *
 * @param string
 * @return string
 */
	public function getFilename() {
		return md5($this->file->name . serialize($this->settings)) . '.' . $this->file->ext();
	}

/**
 * Generate thumbnail image
 *
 * @param array
 * @return string
 */
	public function generateThumbnail($settings = array()) {
		$this->settings = $settings;
		$filename = $this->getFilename();
		$path = $this->getThumbnailPath($filename);

		if (!file_exists($path)) {
			$this->generate($path);
		}

		return $this->getThumbnailUrl();
	}

/**
 * 
 *
 * @param string
 */
	protected function generate($path) {
		foreach ($settings as $k => $v) {
			switch ($k) {
				case 'resize':
					$Factory->resize($v['width'], $v['height']);
					break;
				case 'resizePercent':
					$Factory->resizePercent($v['width'], $v['height']);
					break;
				case 'adaptiveResize':
					$Factory->adaptiveResize($v['width'], $v['height']);
					break;
				case 'cropFromCenter':
					$Factory->cropFromCenter($v['width'], $v['height']);
					break;
				case 'crop':
					$Factory->crop($v['x'], $v['y'], $v['width'], $v['height']);
					break;
				case 'rotateImage':
					$Factory->rotateImage($v);
					break;
				case 'rotateImageNDegrees':
					$Factory->rotateImageNDegrees($v);
					break;
			}
		}

		$Factory->save($path);
	}
}
