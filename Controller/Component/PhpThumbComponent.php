<?php

App::uses('Component', 'Controller');

class PhpThumbComponent extends Component {

/**
 * Array of default options used by the component
 *
 * @var array
 */
	public $settings = array(
		'basePath' => 'files'
	);

/**
 * PhpThumbFactory instance
 *
 * @var PhpThumbFactory
 */
	protected $factory;

/**
 * Constructor
 *
 * @param array $options Options passed by the controller
 * @return void
 */
	public function __construct(ComponentCollection $collection, $settings = array()) {
		$this->settings = Set::merge($this->settings, $settings);
		parent::__construct($collection, $this->settings);
	}

/**
 * Create a new PhpThumbFactory instance
 *
 * @param string $path Path to image file
 * @return PhpThumbFactory
 */
	public function create($path) {
		return PhpThumbFactory::create(WWW_ROOT . 'files' . DS . $path);
	}

/**
 * Magic method get
 *
 * @param string $name Method name to call
 * @param array $arguments Array of arguments
 * @return mixed
 */
	public function __call($name, $arguments = array()) {
		return call_user_func_array(array($this->factory, $name), $arguments);
	}

}
