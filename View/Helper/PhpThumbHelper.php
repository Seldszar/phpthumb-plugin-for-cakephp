<?php

App::uses('File', 'Utility');
App::uses('Thumbnail', 'PhpThumb.Lib');

class PhpThumbHelper extends Helper {

	public $helpers = array('Html');

/**
 * 
 *
 * @param string
 * @param array
 * @param array
 * @return string
 */
	public function thumbnail($path, $thumbSettings = array(), $htmlSettings = array()) {
		//$path = $this->assetUrl($path, $htmlSettings + array('pathPrefix' => IMAGES_URL));
		//$htmlSettings = array_diff_key($htmlSettings, array('fullBase' => '', 'pathPrefix' => ''));
		$thumb = new Thumbnail(IMAGES . $path);
		return $this->Html->image($thumb->generateThumbnail($thumbSettings), $htmlSettings);
	}
}